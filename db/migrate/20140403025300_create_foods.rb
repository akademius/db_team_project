class CreateFoods < ActiveRecord::Migration
  def change
    create_table :foods do |t|
      t.string :name
      t.float :cost
      t.float :price
      t.string :description

      t.timestamps
    end
  end
end
