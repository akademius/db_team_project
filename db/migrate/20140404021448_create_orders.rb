class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.float :total_price
      t.integer :desk_id
      t.integer :member_id

      t.timestamps
    end
  end
end
