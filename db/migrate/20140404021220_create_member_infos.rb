class CreateMemberInfos < ActiveRecord::Migration
  def change
    create_table :member_infos do |t|
      t.string :first_name
      t.string :last_name
      t.string :middle_name
      t.string :address
      t.string :city
      t.string :state
      t.string :zipcode
      t.text :bio
      t.integer :avatar_id

      t.timestamps
    end
  end
end
