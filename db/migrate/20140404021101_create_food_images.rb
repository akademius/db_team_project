class CreateFoodImages < ActiveRecord::Migration
  def change
    create_table :food_images do |t|
      t.integer :image_id
      t.integer :image_set_id

      t.timestamps
    end
  end
end
