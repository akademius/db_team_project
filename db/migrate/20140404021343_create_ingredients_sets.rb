class CreateIngredientsSets < ActiveRecord::Migration
  def change
    create_table :ingredients_sets do |t|
      t.integer :food_id
      t.integer :ingredient_id

      t.timestamps
    end
  end
end
