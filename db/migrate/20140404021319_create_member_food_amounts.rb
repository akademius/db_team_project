class CreateMemberFoodAmounts < ActiveRecord::Migration
  def change
    create_table :member_food_amounts do |t|
      t.integer :member_id
      t.integer :food_id
      t.integer :amount

      t.timestamps
    end
  end
end
