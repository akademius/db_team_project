class CreateImageSets < ActiveRecord::Migration
  def change
    create_table :image_sets do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
