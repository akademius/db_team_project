class CreateIngredients < ActiveRecord::Migration
  def change
    create_table :ingredients do |t|
      t.string :name
      t.float :cost
      t.float :price
      t.text :description

      t.timestamps
    end
  end
end
