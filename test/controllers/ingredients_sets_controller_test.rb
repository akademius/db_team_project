require 'test_helper'

class IngredientsSetsControllerTest < ActionController::TestCase
  setup do
    @ingredients_set = ingredients_sets(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ingredients_sets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ingredients_set" do
    assert_difference('IngredientsSet.count') do
      post :create, ingredients_set: { food_id: @ingredients_set.food_id, ingredient_id: @ingredients_set.ingredient_id }
    end

    assert_redirected_to ingredients_set_path(assigns(:ingredients_set))
  end

  test "should show ingredients_set" do
    get :show, id: @ingredients_set
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ingredients_set
    assert_response :success
  end

  test "should update ingredients_set" do
    patch :update, id: @ingredients_set, ingredients_set: { food_id: @ingredients_set.food_id, ingredient_id: @ingredients_set.ingredient_id }
    assert_redirected_to ingredients_set_path(assigns(:ingredients_set))
  end

  test "should destroy ingredients_set" do
    assert_difference('IngredientsSet.count', -1) do
      delete :destroy, id: @ingredients_set
    end

    assert_redirected_to ingredients_sets_path
  end
end
