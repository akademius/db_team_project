require 'test_helper'

class FoodImagesControllerTest < ActionController::TestCase
  setup do
    @food_image = food_images(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:food_images)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create food_image" do
    assert_difference('FoodImage.count') do
      post :create, food_image: { image_id: @food_image.image_id, image_set_id: @food_image.image_set_id }
    end

    assert_redirected_to food_image_path(assigns(:food_image))
  end

  test "should show food_image" do
    get :show, id: @food_image
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @food_image
    assert_response :success
  end

  test "should update food_image" do
    patch :update, id: @food_image, food_image: { image_id: @food_image.image_id, image_set_id: @food_image.image_set_id }
    assert_redirected_to food_image_path(assigns(:food_image))
  end

  test "should destroy food_image" do
    assert_difference('FoodImage.count', -1) do
      delete :destroy, id: @food_image
    end

    assert_redirected_to food_images_path
  end
end
