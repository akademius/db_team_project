require 'test_helper'

class MemberInfosControllerTest < ActionController::TestCase
  setup do
    @member_info = member_infos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:member_infos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create member_info" do
    assert_difference('MemberInfo.count') do
      post :create, member_info: { address: @member_info.address, avatar_id: @member_info.avatar_id, bio: @member_info.bio, city: @member_info.city, first_name: @member_info.first_name, last_name: @member_info.last_name, middle_name: @member_info.middle_name, state: @member_info.state, zipcode: @member_info.zipcode }
    end

    assert_redirected_to member_info_path(assigns(:member_info))
  end

  test "should show member_info" do
    get :show, id: @member_info
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @member_info
    assert_response :success
  end

  test "should update member_info" do
    patch :update, id: @member_info, member_info: { address: @member_info.address, avatar_id: @member_info.avatar_id, bio: @member_info.bio, city: @member_info.city, first_name: @member_info.first_name, last_name: @member_info.last_name, middle_name: @member_info.middle_name, state: @member_info.state, zipcode: @member_info.zipcode }
    assert_redirected_to member_info_path(assigns(:member_info))
  end

  test "should destroy member_info" do
    assert_difference('MemberInfo.count', -1) do
      delete :destroy, id: @member_info
    end

    assert_redirected_to member_infos_path
  end
end
