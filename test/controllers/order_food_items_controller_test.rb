require 'test_helper'

class OrderFoodItemsControllerTest < ActionController::TestCase
  setup do
    @order_food_item = order_food_items(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:order_food_items)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create order_food_item" do
    assert_difference('OrderFoodItem.count') do
      post :create, order_food_item: { food_id: @order_food_item.food_id, order_id: @order_food_item.order_id }
    end

    assert_redirected_to order_food_item_path(assigns(:order_food_item))
  end

  test "should show order_food_item" do
    get :show, id: @order_food_item
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @order_food_item
    assert_response :success
  end

  test "should update order_food_item" do
    patch :update, id: @order_food_item, order_food_item: { food_id: @order_food_item.food_id, order_id: @order_food_item.order_id }
    assert_redirected_to order_food_item_path(assigns(:order_food_item))
  end

  test "should destroy order_food_item" do
    assert_difference('OrderFoodItem.count', -1) do
      delete :destroy, id: @order_food_item
    end

    assert_redirected_to order_food_items_path
  end
end
