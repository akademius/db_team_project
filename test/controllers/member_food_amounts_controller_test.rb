require 'test_helper'

class MemberFoodAmountsControllerTest < ActionController::TestCase
  setup do
    @member_food_amount = member_food_amounts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:member_food_amounts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create member_food_amount" do
    assert_difference('MemberFoodAmount.count') do
      post :create, member_food_amount: { amount: @member_food_amount.amount, food_id: @member_food_amount.food_id, member_id: @member_food_amount.member_id }
    end

    assert_redirected_to member_food_amount_path(assigns(:member_food_amount))
  end

  test "should show member_food_amount" do
    get :show, id: @member_food_amount
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @member_food_amount
    assert_response :success
  end

  test "should update member_food_amount" do
    patch :update, id: @member_food_amount, member_food_amount: { amount: @member_food_amount.amount, food_id: @member_food_amount.food_id, member_id: @member_food_amount.member_id }
    assert_redirected_to member_food_amount_path(assigns(:member_food_amount))
  end

  test "should destroy member_food_amount" do
    assert_difference('MemberFoodAmount.count', -1) do
      delete :destroy, id: @member_food_amount
    end

    assert_redirected_to member_food_amounts_path
  end
end
