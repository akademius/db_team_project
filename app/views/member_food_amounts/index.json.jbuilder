json.array!(@member_food_amounts) do |member_food_amount|
  json.extract! member_food_amount, :id, :member_id, :food_id, :amount
  json.url member_food_amount_url(member_food_amount, format: :json)
end
