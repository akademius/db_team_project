json.array!(@order_food_items) do |order_food_item|
  json.extract! order_food_item, :id, :food_id, :order_id
  json.url order_food_item_url(order_food_item, format: :json)
end
