json.array!(@ingredients) do |ingredient|
  json.extract! ingredient, :id, :name, :cost, :price, :description
  json.url ingredient_url(ingredient, format: :json)
end
