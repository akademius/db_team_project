json.array!(@food_images) do |food_image|
  json.extract! food_image, :id, :image_id, :image_set_id
  json.url food_image_url(food_image, format: :json)
end
