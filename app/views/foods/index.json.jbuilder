json.array!(@foods) do |food|
  json.extract! food, :id, :name, :cost, :price, :description
  json.url food_url(food, format: :json)
end
