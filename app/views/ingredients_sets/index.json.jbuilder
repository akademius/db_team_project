json.array!(@ingredients_sets) do |ingredients_set|
  json.extract! ingredients_set, :id, :food_id, :ingredient_id
  json.url ingredients_set_url(ingredients_set, format: :json)
end
