json.array!(@member_infos) do |member_info|
  json.extract! member_info, :id, :first_name, :last_name, :middle_name, :address, :city, :state, :zipcode, :bio, :avatar_id
  json.url member_info_url(member_info, format: :json)
end
