json.extract! @member_info, :id, :first_name, :last_name, :middle_name, :address, :city, :state, :zipcode, :bio, :avatar_id, :created_at, :updated_at
