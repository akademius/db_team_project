json.array!(@desks) do |desk|
  json.extract! desk, :id, :name, :capacity, :description
  json.url desk_url(desk, format: :json)
end
